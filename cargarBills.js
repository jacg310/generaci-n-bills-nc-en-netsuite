//Function that generates the full URL for the API Add Record call
//params:
//  (string) URL: base url
//  (string) table: table id
//  (array) fieldMap: array of pair fids/values
// 	(string) ticket: QB ticket
//  (string) token: QB app token
//  (string) op: QB API call

//return: (string) baseURL: Full URL for the API call
function generateURL(URL,table,fieldMap,ticket,token,op) {
	
	var baseURL = URL + table + op;
	
	for (var i = 0; i <  fieldMap.length;i++) {
		baseURL += fieldMap[i][0] +  fieldMap[i][1];
	}
	
	baseURL += '&ticket=' + ticket + '&apptoken=' + token;
	
	return baseURL;
}	


	
	var result;
	// Esta seria la estructura que tendria el objeto
	function VendorBill(recordtype, entity,tranid,RID,vProyecto,user, expense,fecha,vSegmento,vLocation,vMoneda)		
	{
	//	var fTemp = fecha.split('-');
		
	    this.recordtype = recordtype;
	    this.entity = entity;
	    this.tranid = tranid;
	  //  this.tranid = 8742492;
	    this.expense = expense;
	    this.externalid = RID;
	    this.custbody18 = vProyecto;
	    this.custbody_creadoqb = 'T';
	    this.custbody_user_qb = user;
	    this.custbody_cseg_proyecto_const = vSegmento;
	    this.custbody19 = vLocation;
	    this.currency = vMoneda;
	    this.trandate = fecha;

	}
	
	function VendorMemo(recordtype, entity,tranid,RID,vProyecto,user, expense,fecha,vSegmento,vLocation,vMoneda)		
	{
	//	var fTemp = fecha.split('-');
		
	    this.recordtype = recordtype;
	    this.entity = entity;
	    this.tranid = tranid;
	  //  this.tranid = 8742492;
	    this.expense = expense;
	    this.externalid = RID;
	    this.custbody18 = vProyecto;
	    this.custbody_creadoqb = 'T';
	    this.custbody_user_qb = user;
	    this.custbody_cseg_proyecto_const = vSegmento;
	    this.custbody19 = vLocation;
	    this.currency = vMoneda;
	    this.trandate = fecha;

	}
	
	function VendorBillSP(recordtype, entity,tranid,RID,vProyecto,user, expense,fecha,vSegmento,vLocation,vMoneda,proveedor,oc,company,movimiento)		
	{
	//	var fTemp = fecha.split('-');
		
	    this.recordtype = recordtype;
	    this.entity = entity;
	    this.tranid = 'SP-' + tranid;
	    this.expense = expense;
	    this.custbody_rec_sp_qb = RID;
	    this.custbody18 = vProyecto;
	    this.custbody_creadoqb = 'T';
	    this.custbody_user_qb = user;
	    this.custbody_cseg_proyecto_const = vSegmento;
	    this.custbody19 = vLocation;
	    this.currency = vMoneda;
	    this.trandate = fecha;
	    this.memo = proveedor + '-' + 'Movimiento:' + tranid + '-Orden de compra:' + oc;
	    this.company = company;
	    

		

	}
	
	
	function calculoCtasAdicionalesLR(idCta,monto,idProyecto,proveedor){
		
		var lineaRecibo =new Array();
		lineaRecibo[0] = idCta; //Account
		lineaRecibo[1] = String(parseFloat(monto).toFixed(2) * -1); //Amount
		lineaRecibo[2] = idProyecto;

		return lineaRecibo;	
	}
	
	function postData(data) {
		var headers = new Array();
		headers['Content-type'] = 'application/json';
		headers['Authorization'] = 'NLAuth nlauth_email=templates@smartstrategycr.com, nlauth_signature=Templates.2017, nlauth_account=4105920, nlauth_role=3';
		return nlapiRequestURL('https://rest.na1.netsuite.com/app/site/hosting/restlet.nl?script=69&deploy=1', data,headers,null,'POST');

	}
	
	
	
function scheduled() {
	


	//Get credentials information
	var file = nlapiLoadFile('544');
	var encryptedContent = file.getValue();
	var credentials = nlapiDecrypt(encryptedContent,'aes', '43306d706c3358703424247730724421');	
	var credentialsParams = credentials.split(',');
	
	var user = credentialsParams[1];
	var pass = credentialsParams[3];
	var token = credentialsParams[5];	

    // Sets the QB's Base URL and Authentication API call
	var baseURL =  'https://edica.quickbase.com/db/';
	var authURL = 'https://edica.quickbase.com/db/main?a=API_Authenticate&username=' + user +
		 '&password=' +pass + '&hours=12';
	
	//Perforns the api call and obtains the response XML
	var authResponse = nlapiRequestURL(authURL, null, null, null);
	var authXML =  nlapiStringToXML(authResponse.getBody()); 
	
	//Obtains the ticket returned by QB
	var ticket = nlapiSelectValue(authXML, '//ticket');
	
	genBills(token,ticket,baseURL,authURL);
	genSP(token,ticket,baseURL,authURL);

}

function genBills(token,ticket,baseURL,authURL) {
	var doQueryLinesAPI = baseURL + 'bjzq35y7b' + '?a=API_DoQuery&qid=28' 
	 + '&clist=3.138.6.143.87.7.157.142.106.136.145'+
	 '&appToken=' + token + '&ticket=' + ticket;
	var queryLinesResponse = nlapiRequestURL(doQueryLinesAPI, null, null, null);
		
	var queryLinesXML =  nlapiStringToXML(queryLinesResponse.getBody()); 
	
	//Gets the rids and line IDs (if any) from the QB response
	var rids = nlapiSelectValues(queryLinesXML, '//record_id_');
		
	for (var w = 0; w < rids.length; w++) {


		var RID = rids[w];
		var ID_Proveedor= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/ns_proveedor_id');
		var No_Fact= nlapiSelectValue(queryLinesXML, '//record[' +(w + 1) + ']/no_factura');
		var proyecto= nlapiSelectValue(queryLinesXML, '//record[' +( w + 1) + ']/ns_proyecto_id');
		var proyectoSegmento= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/ns_proyectosegmento_id');
		var user= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1 )+ ']/usuario_actual');
		var trandate= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/fecha_factura');
		var location= nlapiSelectValue(queryLinesXML, '//record[' +( w + 1) + ']/orden_de_compra___proyecto___ubicacion_ns');
		var moneda= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/moneda_rd');
		var tipo_doc= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/tipo_documento_rd');
		
		trandate = new Date(parseInt(trandate));
		trandate.setDate(trandate.getDate() + 1);
		
		var strDate = trandate.getDate() + '/' + (trandate.getMonth() + 1) + '/' + trandate.getFullYear();
		
		
		var finalLocation = '';
		var finalMoneda = '';

		switch (location){
			case 'Edica General CR':
				finalLocation = '2';
			break;
			
			case 'Edica General NI':
				finalLocation = '1';
			break;
		}

		switch (moneda){
		case 'USD':
			finalMoneda = '2';
		break;

		case 'CRC':
			finalMoneda = '1';
		break;
		}
		
						
			var doQueryLinesVB = baseURL + 'bjzq35y7d' + '?a=API_DoQuery&query={\'8\'.EX.\'' + RID + '\'}' 
			 + '&clist=3.73.34.37.32.33.61.40.38.42.35.45.59'+
			 '&appToken=' + token + '&ticket=' + ticket;
			var queryLinesResponseVB = nlapiRequestURL(doQueryLinesVB, null, null, null);
			var queryLinesXMLVB =  nlapiStringToXML(queryLinesResponseVB.getBody()); 
	
			var line_rids = nlapiSelectValues(queryLinesXMLVB, '//record_id_');
			nlapiLogExecution('DEBUG', 'LENGTH', 'LENGTH: '+ line_rids.length + ' ' + line_rids[0] + ' '  + line_rids[1])
				
		var lineasRecibo =new Array();
			
			for (var i = 0; i < line_rids.length; i++){		
			//for(var i = 0; i < records.length; i++){
				var cta_id = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1) + ']/ns_cuenta_contable');
				var monto_trans = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1)+ ']/monto_de_transporte_asignado');
				var memo = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1) + ']/detalle_de_orden_compra___impuesto_consumo');
				var desc = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1)+ ']/calculo_descuento');
				var imp_con = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1) + ']/calculo_impconsumo');
				var imp_ven = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1)+ ']/calculo_impventas');
				var sub_desc = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1) + ']/calculo_subtotal_1');
				var sub_2 = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1)+ ']/calculo_subtotal_2');
				var sub_ini = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1)+ ']/calculo_monto__ca_pre_');
				var total = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1)+ ']/calculo_linea_con_transporte');

				
				var insert = false;
				for(var j = 0; j < lineasRecibo.length; j++){
	
					if(cta_id == lineasRecibo[j][0])
					{
					lineasRecibo[j][1] += parseFloat(sub_ini);//Subtotal
					lineasRecibo[j][3] += parseFloat(desc);//Descuento
					lineasRecibo[j][4] += parseFloat(sub_2);//Compra Neta// Debe segregarse en dos campos distintos
					lineasRecibo[j][6] += parseFloat(imp_con);//Impuesto Consumo
					lineasRecibo[j][8] += parseFloat(total);// Total Importe
					lineasRecibo[j][10] += parseFloat(imp_ven);// IVA Compras
					lineasRecibo[j][11] += parseFloat(sub_desc);// Subtotal con descuento
					lineasRecibo[j][12] += parseFloat(monto_trans);// Monto transporte


					insert = true;
					}
				}
				if(insert == false){
					var lineArray = new Array();

					lineArray[0] = cta_id;
					lineArray[1] = parseFloat(sub_ini); //Subtotal
					lineArray[3] = parseFloat(desc);//Descuento
					lineArray[4] = parseFloat(sub_2);
					lineArray[6] = parseFloat(imp_con);//Impuesto consumo
					lineArray[7] = memo;//memo
					lineArray[8] = parseFloat(total);//Total Importe
					lineArray[10] = parseFloat(imp_ven);// IVA Compras
					lineArray[11] = parseFloat(sub_desc);// Subtotal con descuento
					lineArray[12] = parseFloat(monto_trans);// Monto transporte
					lineArray[9] = "5"; //Tax Code
					lineasRecibo.push(lineArray);
				}
			
			}

			
			for(var  i = 0; i < lineasRecibo.length;i++){

				var map = {
					account: lineasRecibo[i][0],
					custcol_d151_subtotal: lineasRecibo[i][1],
//					custcol1: lineasRecibo[i][2],
					custcol2: lineasRecibo[i][3],
					custcol13: lineasRecibo[i][5],
					custcol_impuesto_consumo: lineasRecibo[i][6],
					memo: lineasRecibo[i][7],
					amount: String(lineasRecibo[i][8]),
					taxcode: lineasRecibo[i][9],
					custcol12: lineasRecibo[i][10],
					location: finalLocation,
				    custcol_cseg_proyecto_const : proyectoSegmento,
					custcol14: lineasRecibo[i][11],
					custcol_d151_fletes: lineasRecibo[i][12],
					department: '3'

				};

				lineasRecibo[i] = map;
				}
		if (tipo_doc != 'NC') 
			var vendorbill = new VendorBill('vendorbill',ID_Proveedor,No_Fact,RID,proyecto,user,lineasRecibo,strDate,proyectoSegmento,finalLocation,finalMoneda);
		else
			var vendorbill = new VendorMemo('vendorcredit',ID_Proveedor,No_Fact,RID,proyecto,user,lineasRecibo,strDate,proyectoSegmento,finalLocation,finalMoneda);

			
		 
		// Creamos un arreglo para almacenarlos
		var listaBill = [];
		 
		listaBill.push(vendorbill);
		
		var vendorbillJSON = JSON.stringify(listaBill[0]);
		nlapiLogExecution('DEBUG', 'JSON', vendorbillJSON)
		var json = JSON.parse(vendorbillJSON);
		//alert(vendorbillJSON);
		
		var result = postData(vendorbillJSON);
		nlapiLogExecution('DEBUG', 'TEST', result.getBody())
		
			var len = result.getBody().length;
			var resultshown = result.getBody().substring(0, len -1);
			
			resultshown = resultshown.substring(1);
				
					
					//Create an array of the mapping between QB's fids and their corresponding NS field values
					var fields = [new Array('&_fid_69=',
							encodeURIComponent(resultshown)),new Array('&_fid_109=',encodeURIComponent(''))];
	
					//Performns an Edit Record API call for the Encabezado Facturas Proveedor table and gets the response XML
					var apiCall = generateURL(baseURL, 'bjzq35y7b', fields, ticket,
							token,'?a=API_EditRecord&rid=' + RID);		
					var editResponse = nlapiRequestURL(apiCall, null, null, null);		
					var addXML =  nlapiStringToXML(editResponse.getBody()); 
	
	}
}
	
function genSP(token,ticket,baseURL,authURL){
	
	var rectype = 'vendorbill';
	var doQueryLinesAPI = baseURL + 'bjzq35y7h' + '?a=API_DoQuery&qid=38' 
	 + '&clist=3.6.167.9.184.59.11.178.177.25.10.8.174.173.53.52.33.111.7.190.202.21.238.18.22.239.19.23.240.20'+
	 '&appToken=' + token + '&ticket=' + ticket;
	var queryLinesResponse = nlapiRequestURL(doQueryLinesAPI, null, null, null);
		
	var queryLinesXML =  nlapiStringToXML(queryLinesResponse.getBody()); 
	
	//Gets the rids and line IDs (if any) from the QB response
	var rids = nlapiSelectValues(queryLinesXML, '//record_id_');
	
	for (var w = 0; w < rids.length; w++) {


		var RID = rids[w];
		var movimiento= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/tipo_movimiento');
		var ID_Proveedor= nlapiSelectValue(queryLinesXML, '//record[' +(w + 1) + ']/orden_de_compra___proveedore___ns_id');
		var No_Fact= nlapiSelectValue(queryLinesXML, '//record[' +( w + 1) + ']/no__factura');
		var proyecto= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/orden_de_compra___codigo_proyecto');
		var user= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1 )+ ']/usuario_actual');
		var trandate= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/fecha_factura');
		var proyectoSegmento= nlapiSelectValue(queryLinesXML, '//record[' +( w + 1) + ']/orden_de_compra___proyecto_ns__id2');
		var ubicacion= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/orden_de_compra___proyecto___ubicacion_ns');
		var oc= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/orden_de_compra___no__orden_compra');
		var memo= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/descripcion');
		var proveedor= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/entregar_cheque_a_nombre_de_');
		var id_ret= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/id___cuenta_retencion');
		var id_ade= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/id__cuenta_adelanto');
		var monto_ret= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/formula_monto_rebajos_de_retenciones');
		var monto_ade= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/formula_monto_rebajos_de_adelanto');
		var total_neto_contrato= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/total_neto_a_pagar_contrato');
		var total_neto_pagar= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/total_neto_a_pagar');
		var moneda= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/tipo_moneda');
		var company= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/orden_de_compra___proyecto___related_compania');
		var id_ccss_ins= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/orden_de_compra___maximum_formula_ns_account');

		
		var id_cta1= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/related_insumo_oprebajo1___cuenta_contable_ns_account');
		var monto_ad1= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/monto_opc1');
		var descr1= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/det_opc1');

		var id_cta2= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/related_insumo_oprebajo2___cuenta_contable_ns_account');
		var monto_ad2= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/monto_opc2');
		var descr2= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/det_opc2');

		var id_cta3= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/related_insumo_oprebajo3___cuenta_contable_ns_account');
		var monto_ad3= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/monto_opc3');
		var descr3= nlapiSelectValue(queryLinesXML, '//record[' + (w + 1) + ']/det_opc3');
		
		var vendorCreditAde = null;


		
		trandate = new Date(parseInt(trandate));
		trandate.setDate(trandate.getDate() + 1);
		
		var strDate = trandate.getDate() + '/' + (trandate.getMonth() + 1) + '/' + trandate.getFullYear();
		var finalLocation = '';
		var finalMoneda = '';

		switch (ubicacion){
			case 'Edica General CR':
				finalLocation = '2';
			break;
			
			case 'Edica General NI':
				finalLocation = '1';
			break;
		}

		switch (moneda){
		case 'USD':
			finalMoneda = '2';
		break;
		case 'NIO':
			finalMoneda = '3';
		break;

		case 'CRC':
			finalMoneda = '1';
		break;
		}
		
		if(movimiento.indexOf('Avances') != -1){

			var doQueryLinesVB = baseURL + 'bjzq35y7i' + '?a=API_DoQuery&query={\'7\'.EX.\'' + RID + '\'}AND{\'101\'.GT.0}' 
			 + '&clist=3.111.106.112.113.108.101.114.107'+
			 '&appToken=' + token + '&ticket=' + ticket;
			var queryLinesResponseVB = nlapiRequestURL(doQueryLinesVB, null, null, null);
			var queryLinesXMLVB =  nlapiStringToXML(queryLinesResponseVB.getBody()); 

			var line_rids = nlapiSelectValues(queryLinesXMLVB, '//record_id_');
			nlapiLogExecution('DEBUG', 'LENGTH', 'LENGTH: '+ line_rids.length + ' ' + line_rids[0] + ' '  + line_rids[1])
			var lineasRecibo =new Array();
			var lineasAdelanto =new Array();
			var z = 0;

			
			for (var i = 0; i < line_rids.length; i++){
					
				//for(var i = 0; i < records.length; i++){
					var cta_id = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1) + ']/detalle_de_orden_compra___insumo___cuenta_contable_ns_account');
					var monto_trans = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1)+ ']/monto_de_transporte_asignado');
					var desc = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1)+ ']/calculo_descuento');
					var imp_con = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1) + ']/calculo_impconsumo');
					var imp_ven = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1)+ ']/calculo_impventas');
					var sub_desc = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1) + ']/calculo_subtotal1');
					var sub_2 = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1)+ ']/calculo_subtotal2');
					var total = nlapiSelectValue(queryLinesXMLVB, '//record[' + (i + 1)+ ']/calculo_total');
					nlapiLogExecution('DEBUG', 'SUBTOTAL 1', sub_desc )
					sub_desc = (sub_desc != null && isNaN(sub_desc) != true)?sub_desc:0;
					nlapiLogExecution('DEBUG', 'SUBTOTAL 1 THEN', sub_desc )


			
				var insert = false;
				for(var j = 0; j < lineasRecibo.length; j++){
					
					if(cta_id == lineasRecibo[j][0])
					{
					lineasRecibo[j][1] += parseFloat(sub_desc);//Subtotal
					lineasRecibo[j][3] += parseFloat(desc);//Descuento
					lineasRecibo[j][4] += parseFloat(sub_2);//Compra Neta// Debe segregarse en dos campos distintos
					if (company != '2') 
						lineasRecibo[j][6] += parseFloat(imp_con);//Impuesto Consumo
					else
						lineasRecibo[j][6] = null;
					lineasRecibo[j][8] += parseFloat(total);// Total Importe
					if (company != '2') 
						lineasRecibo[j][10] += parseFloat(imp_ven);// IVA Compras
					else
						lineasRecibo[j][10] = 0;
					
					lineasRecibo[j][11] += parseFloat(sub_desc);// Subtotal con descuento
					lineArray[12] = parseFloat(monto_trans);// Monto transporte
					
					
					insert = true;
					}
					
					}
					if(insert == false){
					var lineArray = new Array();
					
					lineArray[0] = cta_id;
					lineArray[1] = parseFloat(sub_desc); //Subtotal
					lineArray[3] = parseFloat(desc);//Descuento
					lineArray[4] = parseFloat(sub_2);
					if (company != '2') 
						lineArray[6] = parseFloat(imp_con);//Impuesto consumo
					else
						lineArray[6] = null;
			
					lineArray[8] = parseFloat(total);//Total Importe
					if (company != '2') 
						lineArray[10] = parseFloat(imp_ven);// IVA Compras
					else
						lineArray[10] = 0;
					lineArray[11] = parseFloat(sub_desc);// Subtotal con descuento
					lineArray[12] = parseFloat(monto_trans);// Monto transporte
					if (company != '2') 
						lineArray[9] = "5"; //Tax Code
					else
						lineArray[9] = "22"; //Tax Code
					lineasRecibo.push(lineArray);
					}
					
					}
			
			
			
			for(var  i = 0; i < lineasRecibo.length;i++){
			
			var map = {
				account: lineasRecibo[i][0],
				custcol_d151_subtotal: lineasRecibo[i][1],
			//	custcol1: lineasRecibo[i][2],
				custcol2: lineasRecibo[i][3],
				custcol13: lineasRecibo[i][5],
				custcol_impuesto_consumo: lineasRecibo[i][6],
				memo: proveedor + '-' + 'Movimiento:' + No_Fact + '-Orden de compra:' + oc,
				amount: String(lineasRecibo[i][8]),
				taxcode: lineasRecibo[i][9],
				custcol12: lineasRecibo[i][10],
				location: finalLocation,
			    custcol_cseg_proyecto_const : proyectoSegmento,
				custcol14: lineasRecibo[i][11],
		//		custcol_d151_fletes: lineasRecibo[i][12],
				custcol_d151_fletes: 0,
				department: '3',
				custcol_4601_witaxapplies:'T'
			
			};
			
			lineasRecibo[i] = map;
			}
			
			if(parseFloat(monto_ade) > 0 && company != '2'){

				
			var map = {
					account: id_ade,
					memo: proveedor + '-' + 'Movimiento de Adelanto:' + No_Fact + '-Orden de compra:' + oc,
					amount: String(parseFloat(monto_ade).toFixed(2)),
					custcol_d151_subtotal: String(parseFloat(monto_ade).toFixed(2)),
					location: finalLocation,
				    custcol_cseg_proyecto_const : proyectoSegmento,
					taxcode: '5',
					department: '3'
				};
				
				lineasAdelanto[z] = map;
				z++;

		
						
			}
			 if(parseFloat(monto_ret) > 0 && company != '2'){
					 var map =		{
							account: id_ret,
							custcol_d151_subtotal:String(parseFloat(monto_ret).toFixed(2)),
							amount: String(parseFloat(monto_ret).toFixed(2)),
							location: finalLocation,
							custcol_cseg_proyecto_const : proyectoSegmento,
							memo: proveedor + '-' + 'Movimiento:' + No_Fact + '-Orden de compra:' + oc,
							taxcode: '5',
							department: '3'
						};
			nlapiLogExecution('DEBUG', 'VALUES', id_cta1 + ' ' + id_cta2 + ' ' + id_cta3);
			
			lineasAdelanto[z] = map;
			z++;

	
			
			}
	
			if(parseFloat(id_cta1) > 0 && id_cta1 != null && company != '2')
							lineasRecibo.push(
									{
										account: id_cta1,
										custcol_d151_subtotal:String(parseFloat(monto_ad1).toFixed(2) * -1),
										amount: String(parseFloat(monto_ad1).toFixed(2) * -1),
										location: finalLocation,
										custcol_cseg_proyecto_const : proyectoSegmento,
										memo: descr1,
										taxcode: '5',
										department: '3'
									});
						if(parseFloat(id_cta2) > 0 && id_cta2 != null && company != '2')
							lineasRecibo.push(
									{
										account: id_cta2,
										custcol_d151_subtotal:String(parseFloat(monto_ad2).toFixed(2) * -1),
										amount: String(parseFloat(monto_ad2).toFixed(2) * -1),
										location: finalLocation,
										custcol_cseg_proyecto_const : proyectoSegmento,
										memo: descr2,
										taxcode: '5',
										department: '3'
									});
						if(parseFloat(id_cta3) > 0 && id_cta3 != null && company != '2')
							lineasRecibo.push(
									{
										account: id_cta3,
										custcol_d151_subtotal:String(parseFloat(monto_ad3).toFixed(2) * -1),
										amount: String(parseFloat(monto_ad3).toFixed(2) * -1),
										location: finalLocation,
										custcol_cseg_proyecto_const : proyectoSegmento,
										memo: descr3,
										taxcode: '5',
										department: '3'
									});
		}// END TIPO MOVIMIENTO AVANCE
		
		else if(movimiento.indexOf('CCSS') !=-1 || movimiento.indexOf('INS') !=-1 ){
			var lineasRecibo =new Array();
			var map = {
					account: id_ccss_ins,
					memo: proveedor + '-' + 'Movimiento de CCSS/INS:' + No_Fact + '-Orden de compra:' + oc,
					amount: String(total_neto_pagar),
					custcol_d151_subtotal: String(total_neto_contrato),
					custcol14: String(total_neto_contrato),
					location: finalLocation,
				    custcol_cseg_proyecto_const : proyectoSegmento,
				    taxcode: '5',
					department: '3'		
				};
				
				lineasRecibo[0] = map;
		}
		else if (movimiento.indexOf('Adelantos') !=-1) {
			var lineasRecibo =new Array();
			var map = {
					account: '844',
					memo: proveedor + '-' + 'Movimiento de Adelanto:' + No_Fact + '-Orden de compra:' + oc,
					amount: String(total_neto_contrato),
					custcol_d151_subtotal: String(total_neto_contrato),
					custcol14: String(total_neto_contrato),
					location: finalLocation,
				    custcol_cseg_proyecto_const : proyectoSegmento,
				    taxcode: '5',
					department: '3'		
				};
				
				lineasRecibo[0] = map;
				rectype = 'vendorcredit';
		}
		else if (movimiento.indexOf('Devoluciones Retenciones') !=-1) {
			var lineasRecibo =new Array();
			var map = {
					account: '324',
					memo: proveedor + '-' + 'Movimiento de Retencion:' + No_Fact + '-Orden de compra:' + oc,
					amount: String(total_neto_contrato),
					custcol_d151_subtotal: String(total_neto_contrato),
					custcol14: String(total_neto_contrato),
					location: finalLocation,
				    custcol_cseg_proyecto_const : proyectoSegmento,
				    taxcode: '5',
					department: '3'	
				};
				
				lineasRecibo[0] = map;
				rectype = 'vendorcredit';
		}


		var vendorbill = new VendorBillSP(rectype,ID_Proveedor,No_Fact,RID,proyecto,user,lineasRecibo,strDate,proyectoSegmento,
											finalLocation,finalMoneda,proveedor,oc,company,movimiento);
		
		
		
		// Creamos un arreglo para almacenarlos
		var listaBill = [];
		 
		listaBill.push(vendorbill);
		
		var vendorbillJSON = JSON.stringify(listaBill[0]);
		nlapiLogExecution('DEBUG', 'JSON', vendorbillJSON)
		var json = JSON.parse(vendorbillJSON);
		//alert(vendorbillJSON);
		
		var result = postData(vendorbillJSON);
		nlapiLogExecution('DEBUG', 'TEST', result.getBody())
		
			var len = result.getBody().length;
			var resultshown = result.getBody().substring(0, len -1);
			
			resultshown = resultshown.substring(1);
			

			
				
					
					//Create an array of the mapping between QB's fids and their corresponding NS field values
					var fields = [new Array('&_fid_175=',
							encodeURIComponent(resultshown)),new Array('&_fid_186=',encodeURIComponent(''))];
	
					//Performns an Edit Record API call for the Encabezado Facturas Proveedor table and gets the response XML
					var apiCall = generateURL(baseURL, 'bjzq35y7h', fields, ticket,
							token,'?a=API_EditRecord&rid=' + RID);		
					var editResponse = nlapiRequestURL(apiCall, null, null, null);		
					var addXML =  nlapiStringToXML(editResponse.getBody()); 
		
					vendorCreditAde = new VendorBillSP('vendorcredit',ID_Proveedor,No_Fact,RID,proyecto,user,lineasAdelanto,strDate,proyectoSegmento,
		 					finalLocation,finalMoneda,proveedor,oc,company,movimiento);	
					
			if(!isNaN(resultshown) && vendorCreditAde.expense != null && typeof vendorCreditAde.expense != undefined){
							 
			
			
				var Adelanto = [];
				var bill_id = resultshown;
				Adelanto.push(vendorCreditAde);

				var vendorAdeJSON = JSON.stringify(Adelanto[0]);
				nlapiLogExecution('DEBUG', 'JSON', vendorAdeJSON)
				var json = JSON.parse(vendorAdeJSON);

				var resultAde = postData(vendorAdeJSON);

				var len = resultAde.getBody().length;
				var resultshown = resultAde.getBody().substring(0, len -1);

				resultshown = resultshown.substring(1);
				nlapiLogExecution('DEBUG', 'RESULTSHOWN', resultshown);
				if(!isNaN(resultshown)){

					var bill = nlapiLoadRecord('vendorbill', bill_id);
					bill.setFieldValue('custbody_nc_relacionada',resultshown);
					nlapiSubmitRecord(bill,null,true);

					
				}
				
									//Create an array of the mapping between QB's fids and their corresponding NS field values
					var fields = [new Array('&_fid_274=',
							encodeURIComponent(resultshown))];
	
					//Performns an Edit Record API call for the Encabezado Facturas Proveedor table and gets the response XML
					var apiCall = generateURL(baseURL, 'bjzq35y7h', fields, ticket,
							token,'?a=API_EditRecord&rid=' + RID);		
					var editResponse = nlapiRequestURL(apiCall, null, null, null);		
					var addXML =  nlapiStringToXML(editResponse.getBody()); 
				
			
			}
		
	}
	
}
